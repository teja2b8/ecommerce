from django.db import models
from products.models import Product

# Create your models here.
class Tag(models.Model):
	title = models.CharField(max_length=100)
	slug = models.SlugField()
	timestamp = models.DateField(auto_now_add=True)
	active = models.BooleanField(default=True)
	products = models.ManyToManyField(Product, blank=True)

	def __str__(self):
		return self.title

# def pre_save_receiver(sender, instance, *args, **kwargs):
# 	if not instance:
# 		instance.slug = unique_slug_generator(instance)

# pre_save.connect(pre_save_receiver, sender=Tag)