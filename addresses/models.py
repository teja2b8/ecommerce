from django.db import models

from billing.models import BillingProfile
# Create your models here.

ADDRESSES_TYPES = (
	('billing', 'Billing'),
	('shipping', 'Shipping')
	)

class Address(models.Model):
	billing_profile = models.ForeignKey(BillingProfile, on_delete=models.CASCADE)
	address_type 	= models.CharField(max_length=100, choices=ADDRESSES_TYPES)
	address_line_2 	= models.CharField(max_length=100)
	address_line_1 	= models.CharField(max_length=100, blank=True, null=True)
	city 			= models.CharField(max_length=100)
	state 			= models.CharField(max_length=100)
	country 		= models.CharField(max_length=100)
	postal_code 	= models.CharField(max_length=100)

	def __str__(self):
		return str(self.billing_profile)

	def get_address(self):
		return f"{self.address_line_1}\n{self.address_line_2}\n{self.city}\n{self.state}\n{self.country}\n{self.postal_code}\n"