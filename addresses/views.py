from django.shortcuts import render, redirect
from django.utils.http import is_safe_url

from billing.models import BillingProfile
from .forms import AddressForm
from .models import Address
# Create your views here.

def shipping_address_create(request):
	form = AddressForm(request.POST or None)
	next_ = request.GET.get('next')
	next_post = request.POST.get('next')
	redirect_path = next_ or next_post or None

	if form.is_valid():
		instance = form.save(commit=False)
		billing_profile, created = BillingProfile.objects.new_or_get(request)
		if billing_profile is not None:
			address_type = request.POST.get('address_type', 'shipping')
			instance.billing_profile = billing_profile
			instance.address_type = address_type
			instance.save()
			request.session[address_type + "_address_id"] = instance.id
		else:
			return redirect("carts:checkout")
		if is_safe_url(redirect_path, request.get_host()):
			return redirect(redirect_path)
	return redirect("carts:checkout")

def shipping_address_reuse(request):
	if request.user.is_authenticated:
		if request.method == 'POST':
			next_ = request.GET.get('next')
			next_post = request.POST.get('next')
			redirect_path = next_ or next_post or None
			shipping_address_id = request.POST.get('shipping_address', None)
			billing_profile, created = BillingProfile.objects.new_or_get(request)
			address_type = request.POST.get('address_type', 'shipping')
			if shipping_address_id is not None:
				qs = Address.objects.filter(billing_profile=billing_profile, id=shipping_address_id)
				if qs.exists():
					request.session[address_type + "_address_id"] = shipping_address_id
			if is_safe_url(redirect_path, request.get_host()):
				return redirect(redirect_path)
	return redirect("carts:checkout")
