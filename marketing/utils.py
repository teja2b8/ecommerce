import hashlib
import json
import re
import requests
from django.conf import settings

MAILCHIMP_API_KEY = getattr(settings, 'MAILCHIMP_API_KEY', None)
MAILCHIMP_DATA_CENTER = getattr(settings, 'MAILCHIMP_DATA_CENTER', None)
MAILCHIMP_EMAIL_LIST_ID = getattr(settings, 'MAILCHIMP_EMAIL_LIST_ID', None)

def check_email(email):
    if not re.match(r".+@.+\..", email):
        raise ValueError("String passed is not a valid email")
    return email

def get_subscriber_hash(email):
    check_email(email)
    mem_email = email.lower().encode()
    m =hashlib.md5(mem_email)
    return m.hexdigest()

class MailChimp():
    def __init__(self):
        super(MailChimp, self).__init__()
        self.key = MAILCHIMP_API_KEY
        self.api_url = "https://{dc}.api.mailchimp.com/3.0".format(
                    dc=MAILCHIMP_DATA_CENTER
                    )
        self.list_id = MAILCHIMP_EMAIL_LIST_ID
        self.list_endpoints = f'{self.api_url}/lists/{self.list_id}'

    def get_members_endpoint(self):
        return self.list_endpoints + "/members"

    def change_subscription_status(self, email, status='unsubscribed'):
        hash_email = get_subscriber_hash(email)
        endpoint = self.get_members_endpoint() + '/' + hash_email
        data = {"status":status}
        res = requests.put(endpoint, auth=('', self.key), data=json.dumps(data))
        return res.status_code, res.json()

    def get_subscription_status(self, email,):
        hash_email = get_subscriber_hash(email)
        endpoint = self.get_members_endpoint() + '/' + hash_email 
        res = requests.get(endpoint, auth=('', self.key))
        return res.status_code, res.json()

    def check_valid_status(self, status):
        choices = ['subscribed', 'unsubscribed', 'pending', 'cleaned']
        if not status in choices:
            raise ValueError("status not found")
        return status

    def add_email(self, email):
        # status = "subscribed"
        # self.check_valid_status(status)
        # data = {
        #     "email_address": email,
        #     "status": status
        # }
        # endpoint = self.get_members_endpoint()
        # r = requests.post(endpoint, auth=("", self.key), data=json.dumps(data))
        # return r.json()
        return self.change_subscription_status(email, status='subscribed')

    def subscribe(self, email):
        return self.change_subscription_status(email, status='subscribed')

    def unsubscribe(self, email):
        return self.change_subscription_status(email, status='unsubscribed')

    def pending(self, email):
        return self.change_subscription_status(email, status='pending')


