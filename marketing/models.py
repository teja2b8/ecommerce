from django.db import models
from django.conf import settings
from django.db.models.signals import post_save, pre_save

from .utils import MailChimp

User = settings.AUTH_USER_MODEL

class MarketingPreference(models.Model):
	user 					= models.OneToOneField(User, on_delete=models.CASCADE)
	subscribed 				= models.BooleanField(default=True)
	mailchimp_subscribed 	= models.NullBooleanField(blank=True)
	mailchimp_msg 			= models.TextField(null=True, blank=True)
	created_at 				= models.DateTimeField(auto_now_add=True)
	updated_at 				= models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.user.email

def marketing_pref_create_receiver(sender, instance, created, *args, **kwargs):
	if created:
		status , res_data = MailChimp().subscribe(instance.user.email)
		print(status, res_data)

post_save.connect(marketing_pref_create_receiver, sender=MarketingPreference)

def marketing_pref_update_receiver(sender, instance, *args, **kwargs):
	if instance.subscribed != instance.mailchimp_subscribed:
		if instance.subscribed:
			status, res_data = MailChimp().subscribe(instance.user.email)
		else:
			status, res_data = MailChimp().unsubscribe(instance.user.email)

	if res_data['status'] == 'subscribed':
		instance.subscribed, instance.mailchimp_subscribed = True, True
		instance.mailchimp_msg = res_data
	else:
		instance.subscribed, instance.mailchimp_subscribed = False, False
		instance.mailchimp_msg = res_data

pre_save.connect(marketing_pref_update_receiver, MarketingPreference)

def make_marketing_pref_receiver(sender, instance, created, *args, **kwargs):
	# if user created
	if created:
		MarketingPreference.objects.get_or_create(user=instance)

post_save.connect(make_marketing_pref_receiver, sender=User)