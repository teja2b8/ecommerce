from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from .forms import ContactForm

def home_page(request):
	context = {'title' : ""}
	return render(request, "home_page.html", context)

def about_page(request):
	context = {'title' : "welcome to about page."}
	return render(request, "home_page.html", context)

def contact_page(request):
	form = ContactForm(request.POST or None)
	context = {	'title':'Contact',
				'content' : "Welcome to contact page",
				'form': form }
	if form.is_valid():
		if request.is_ajax():
			return JsonResponse({'message' : 'Thanks for your submission'})
		print(form.cleaned_data)

	if form.errors:
		errors = form.errors.as_json()
		if request.is_ajax():
			return HttpResponse(errors, status=400, content_type='application/json')
	return render(request, "contact/contact.html", context)