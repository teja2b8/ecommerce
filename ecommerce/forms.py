from django.contrib.auth import get_user_model
from django import forms

User = get_user_model()

class ContactForm(forms.Form):
	fullname = forms.CharField(
		max_length=100,
		widget=forms.TextInput(
			attrs={'placeholder':'Full Name'}
			)
		)
	email = forms.EmailField(
		widget=forms.EmailInput(
			attrs={'placeholder':'Email'}
			)
		)
	content = forms.CharField(
		widget=forms.TextInput(
			attrs={'placeholder':"Content"}
			)
		)

	def clean_email(self):
		email = self.cleaned_data.get('email')
		if not '@gmail.com' in email:
			raise forms.ValidationError("only gmail accounts are allowed")
		return email

	# def clean_content(self):
	# 	raise forms.ValidationError("content is wrong")
