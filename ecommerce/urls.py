"""ecommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include
from django.views.generic import TemplateView, RedirectView

from accounts.views import LoginView, RegisterView, guest_register_view, AccountHomeView
from addresses.views import shipping_address_create, shipping_address_reuse
from billing.views import billing_payment_view, billing_payment_create_view
from carts.views import cart_home, cart_detais_api_view
from marketing.views import MarketingPreferenceUpdateView
from products.views import UserProductHistoryView
from .views import home_page, about_page, contact_page

urlpatterns = [
	path('', home_page, name='home'),
	path('about/', about_page, name='about'),
    path('account/', AccountHomeView.as_view(), name='accounts'),
    path('accounts/', RedirectView.as_view(url='/account')),
    path('accounts/', include("accounts.passwords.urls")),
    path('admin/', admin.site.urls),
    path('api/cart/', cart_detais_api_view, name='api_cart'),
    path('billing-payment/', billing_payment_view, name='billing-payment'),
    path('billing-payment/create/', billing_payment_create_view, name='billing-create-payment'),
    path('bootstrap/', TemplateView.as_view(template_name='bootstrap/bootstrap.html'), name='bootstrap'),
    path('cart/', include(("carts.urls", "carts"), namespace='carts')),
	path('contact/', contact_page, name='contact'),
    path('history/products/', UserProductHistoryView.as_view(), name='user-product-history'),
	path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('orders/', include(("orders.urls", "orders"), namespace='orders')),
    path('products/', include(("products.urls", "products"), namespace='products')),
	path('register/', RegisterView.as_view(), name='register'),
    path('register/guest/', guest_register_view, name='guest_register'),
    path('settings/', RedirectView.as_view(url='/account')),
    path('settings/email/', MarketingPreferenceUpdateView.as_view(), name='marketing-preferences'),
    path('shipping/address/create', shipping_address_create, name='address_checkout'),
    path('shipping/address/reuse', shipping_address_reuse, name='address_reuse'),
    path('search/', include(("search.urls", "search"), namespace='search')),
    # path('products_fbv/', include('products.urls')),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
