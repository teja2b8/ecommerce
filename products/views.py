from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.shortcuts import render
from django.views.generic import ListView, DetailView

from analytics.mixins import ObjectViewedMixin
from carts.models import Cart
from .models import Product
# Create your views here.

class UserProductHistoryView(LoginRequiredMixin, ListView):
    template_name = "products/user-history.html"
    def get_context_data(self, *args, **kwargs):
        context = super(UserProductHistoryView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        views = request.user.objectviewed_set.by_model(Product, model_queryset=False)
        return views

class ProductListView(ListView):
	def get_context_data(self, *args, **kwargs):
		context = super(ProductListView, self).get_context_data(*args, **kwargs)
		cart_obj, new_obj = Cart.objects.new_or_get(self.request)
		context['cart'] = cart_obj
		return context

	queryset = Product.objects.all()
	template_name ="products/product_list.html"

class ProductDetailView(ObjectViewedMixin, DetailView):
	def get_context_data(self, *args, **kwargs):
		context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
		cart_obj, new_obj = Cart.objects.new_or_get(self.request)
		context['cart'] = cart_obj
		return context

	def get_object(self, *args, **kwargs):
		request = self.request
		slug = self.kwargs.get('slug')
		try:
			instance = Product.objects.get(slug=slug, active=True)
		except ProductDoesNotExist:
			raise Http404("Not found...")
		except Product.MultipleObjectsReturned:
			qs = Product.objects.filter(slug=slug, active=True)
			instance = qs.first()
		# object_viewd_signal.send(instance.__class__, instance=instance, request=request)
		return instance

	queryset = Product.objects.all()
	template_name = "products/details_list.html"

def product_detail_view(request, slug):
	qs = Product.objects.get_by_id(slug)
	if qs is None:
		raise Http404("Product Not Found")
	context = {'object' : qs}
	return render(request, "products/details_list.html", context)

def product_list_view(request):
	context = {'object_list' : Product.objects.all()}
	return render(request, "products/product_list.html", context)