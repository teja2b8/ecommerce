# import python modules
import os
import random

# import django modules
from django.db import models
from django.urls import reverse
from django.db.models import Q

# Create your models here.
def get_extension(filename):
	basename = os.path.basename(filename)
	name, ext = os.path.splitext(basename)
	return ext

# Return path to store uploaded images
def upload_image_path(instance, filename):
	random_name = random.randint(1,4000000)
	ext = get_extension(filename)
	file_name = f"{random_name}{ext}"
	return f"products/{file_name}"

# over write product queryset
class ProductQuerySet(models.query.QuerySet):

    # Product.objects.featured
	def featured(self):
		return self.filter(featured=True)
    # Product.objects.active
	def active(self):
		return self.filter(active=True)
    # Product.objects.search
	def search(self, query):
		lookups = (	Q(title__icontains=query) |
					Q(description__icontains=query) |
					Q(price__icontains=query) |
					Q(slug__icontains=query))
		print(lookups)
		print(self.filter(lookups))
		print(self.filter(lookups).distinct())
		return self.filter(lookups).distinct()

# Over writting product.objects
class ProductManager(models.Manager):

    # Product.objects.all()
	def all(self):
		return self.get_queryset().filter(active=True)
    # over write queryset of Product class
	def get_queryset(self):
		return ProductQuerySet(self.model, using=self.db)
    # Product.objects.featured()
	def featured(self):
		return self.get_queryset().filter(featured=True)
    # Product.objects.search()
	def search(self, query):
		print(query)
		return self.get_queryset().active().search(query)
    #Product.objects.get_by_id()
	def get_by_id(self, slug):
		qs = self.get_queryset().filter(slug=slug)
		if qs.count() == 1:
			return qs.first()
		return None

# Product Model - Defines the attributes of each item
class Product(models.Model):
	title = models.CharField(max_length=100)
	slug = models.SlugField(default=None, null=True)
	description = models.CharField(max_length=100)
	price = models.DecimalField(decimal_places=2, max_digits=4, default=40)
	image = models.ImageField(upload_to=upload_image_path, null=True)
	featured = models.BooleanField(default=False)
	active = models.BooleanField(default=True)
	timestamp = models.DateField(auto_now=True)

    # Assigning product manager
	objects = ProductManager()

    # Returns url based on the namespace
	def get_absolute_url(self):
		return reverse('products:detail', kwargs={'slug':self.slug})

	def __str__(self):
		return str(self.title)