

from django.contrib import admin
from django.urls import path

from products.views import ProductListView, product_list_view, ProductDetailView, product_detail_view

urlpatterns = [
    path('', ProductListView.as_view(), name='list'),
    # path('', product_list_view),
    path('<slug:slug>/', ProductDetailView.as_view(), name='detail'),
    # path('<slug:slug>/', product_list_viewdetail_view),
]