$(document).ready(function(){

 	var stripeModule = $(".stripe-payment-form")
	var stripeTemplate =  $.templates("#stripeTemplate")
	var stripeToken = stripeModule.attr('data-token')
	var stripeNextUrl = stripeModule.attr('data-next-url')
	var stripeAddTitle = stripeModule.attr('data-btn-title') || "Add Card"
	var stripeTemplateData = {
		pubKey : stripeToken,
		nextUrl : stripeNextUrl,
		addTitle : stripeAddTitle
	}
	var stripeTemplateHtml = stripeTemplate.render(stripeTemplateData)
	stripeModule.html(stripeTemplateHtml)

	// Create a Stripe client.
	paymentForm = $(".payment-form")
	if (paymentForm.length > 1){
		paymentForm.css('display', 'none')
	}
	else if (paymentForm.length == 1){
		pubKey = paymentForm.attr('data-token')
		nextUrl = paymentForm.attr('data-next-url')
		var stripe = Stripe(pubKey);

		var elements = stripe.elements();

		var style = {
		  base: {
		    color: '#32325d',
		    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
		    fontSmoothing: 'antialiased',
		    fontSize: '16px',
		    '::placeholder': {
		      color: '#aab7c4'
		    }
		  },
		  invalid: {
		    color: '#fa755a',
		    iconColor: '#fa755a'
		  }
		};

		// Create an instance of the card Element.
		var card = elements.create('card', {style: style});

		// Add an instance of the card Element into the `card-element` <div>.
		card.mount('#card-element');

		// Handle real-time validation errors from the card Element.
		card.addEventListener('change', function(event) {
		  var displayError = document.getElementById('card-errors');
		  if (event.error) {
		    displayError.textContent = event.error.message;
		  } else {
		    displayError.textContent = '';
		  }
		});

		// Handle form submission.
		var form = document.getElementById('payment-form');
		form.addEventListener('submit', function(event) {
		  event.preventDefault();

		  stripe.createToken(card).then(function(result) {
		    if (result.error) {
		      // Inform the user if there was an error.
		      var errorElement = document.getElementById('card-errors');
		      errorElement.textContent = result.error.message;
		    } else {
		      // Send the token to your server.
		      stripeTokenHandler(result.token);
		    }
		  });
		});
		// var form = $('#payment-form');
		// form.addEventListener('submit', function(event) {
		//   event.preventDefault();
		  

		//   stripe.createToken(card).then(function(result) {
		//     if (result.error) {
		//       // Inform the user if there was an error.
		//       var errorElement = $('#card-errors');
		//       errorElement.textContent = result.error.message;
		//     } else {
		//       // Send the token to your server.
		//       stripeTokenHandler(result.token);
		//     }
		//   });
		// });

		// Submit the form with the token ID.
		function stripeTokenHandler(token) {
			data = {
				'token':token.id
			}
			paymentCreateUrl = "/billing-payment/create/"
			$.ajax({
				data : data,
				url  : paymentCreateUrl,
				method : 'POST',
				success : function(data){
					card.clear()
					$.alert(data.message + "<br><br><i class='fa fa-spin fa-spinner'></i> Redirecting...")
					setTimeout(function(){
						window.location.href = "/cart/checkout/"
					}, 500)
				},
				error:function(error){
					console.log(error)
				}
			}
			)
		}
	}
	})