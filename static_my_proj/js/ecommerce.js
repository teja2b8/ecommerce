$(document).ready(function(){
	var contactForm = $(".contact-form")
	var contactMethod = contactForm.attr('method')
	var contactUrl = contactForm.attr('action')

	function displaySubmit(doSubmit, default_text, submitBtn){
		if(doSubmit){
			submitBtn.addClass('disabled')
			submitBtn.html("<i class='fa fa-spin fa-spinner'></i> Sending...")
		}
		else {
			var contactSubmitTxt = submitBtn.text()
			submitBtn.removeClass('disabled')
			submitBtn.html(contactSubmitTxt)
		}
	}
	contactForm.submit(function(event){
		event.preventDefault()
		var contactData = contactForm.serialize()
		var contactSubmit = contactForm.find("[type='submit']")
		
		$.ajax({
			url : contactUrl,
			method : contactMethod,
			data : contactData,
			success : function(data){
				displaySubmit(true, '', contactSubmit)
				$.alert({
					title:'Success!!',
					content:data.message,
					theme:'modern'
				})
				setTimeout(function(){
					displaySubmit(false, '', contactSubmit)
				}, 500)
				contactForm[0].reset()
			},
			error : function(error){
				console.log(error)
				var jsonData = error.responseJSON
				var msg=""
				$.each(jsonData, function(key, value){
					msg += key + ": " + value[0].message + "</br>"
				})
				$.alert({
					title:'OOps!',
					content:msg,
					theme:'modern'
				})
				setTimeout(function(){
					displaySubmit(false, '', contactSubmit)
				}, 500)
			}
		})
	})

	// serch manager
	var serchForm = $(".search-form")
	var serchInput = serchForm.find("[name='q']")
	var serchSubmit = serchForm.find("[type='submit']")
	var typingTimer
	var typingInterval = 500
	serchInput.keyup(function(event){
		clearTimeout(typingTimer)
		typingTimer = setTimeout(performSearch, typingInterval)
	})

	serchInput.keydown(function(event){
		clearTimeout(typingTimer)
	})

	function displaySearch(){
		serchSubmit.addClass('disabled')
		serchSubmit.html("<i class='fa fa-spin fa-spinner'></i> Searching...")
	}

	function performSearch(){
		displaySearch()
		var query = serchInput.val()
		setTimeout(function(){
			window.location.href = "/search/?q="+query
		}, 1000)
	}

	// cart handling
	var productForm = $(".form-product-ajax")
	productForm.submit(function(event){
		event.preventDefault();
		var thisForm = $(this)
		var actionEnpoint = thisForm.attr("data-endpoint");
		var httpMethod = thisForm.attr("method");
		var formData = thisForm.serialize();
		$.ajax({
			url : actionEnpoint,
			method : httpMethod,
			data : formData,
			success : function(data){
				var submiSpan = thisForm.find(".submit-span")
				if (data.added){
					submiSpan.html('In Cart <button class="btn" type="submit">Remove From Cart</button>')
				}
				else {
					submiSpan.html('<button class="btn btn-success" type="submit">Add To Cart</button>')
				}
				var cart_counter = $(".cart-count-span")
				cart_counter.text(data.cart_count)
				var currentLocation = window.location.href
				if(currentLocation.indexOf('cart') != -1){
					cartRefresh()
				}
			},
			error : function(errorData){
				$.alert({
					title:'OOps!',
					content:'An Error Occured',
					theme:'modern'
				})
			}
		})
	})
	function cartRefresh(){
		var cartTable = $(".cart-table");
		var cartBody = cartTable.find(".cart-body");
		var productRows = cartBody.find(".cart-products");
		var subTotal = cartBody.find(".cart-sub-total");
		var total = cartBody.find(".cart-total");
		var currentUrl = window.location.href
		$.ajax({
			url : '/api/cart/',
			method : 'GET',
			success : function(data){
				var removeButtonForm = $(".cart-remove-form")
				if(data.products.length > 0){
					productRows.html(" ")
					i = data.products.length
					$.each(data.products, function(index, value){
						var removeButton = removeButtonForm.clone()
						removeButton.find(".remove_button").val(value.id)
						removeButton.css("display", "block")
						cartBody.prepend("<tr><th scope=\"row\">" + i + "</th><td><a href='" + value.url + "'>" + value.name + "</a></td><td>" + value.price + "</td><td>" + removeButton.html() + "</td></tr>")
						i --
					})
					subTotal.text(data.sub_total);
					total.text(data.total);
				}
				else{
					window.location.href = currentUrl
				}
			},
			error : function(errorData){
				$.alert({
					title:'OOps!',
					content:'An Error Occured',
					theme:'modern'
				})
			}
		})
	}
	})