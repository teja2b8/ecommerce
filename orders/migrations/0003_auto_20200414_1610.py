# Generated by Django 3.0.4 on 2020-04-14 10:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0002_auto_20200414_0003'),
        ('orders', '0002_auto_20200413_2355'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='acive',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='order',
            name='billing_profile',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='billing.BillingProfile'),
        ),
    ]
