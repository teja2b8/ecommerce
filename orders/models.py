from addresses.models import Address
from billing.models import BillingProfile
from carts.models import Cart
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.shortcuts import reverse
from ecommerce.utils import unique_order_id_generator
from math import fsum

# Create your models here.

ORDER_STATUS_CHOICES = (
    ('created', 'Created'),
    ('paid', 'Paid'),
    ('refunded', 'Refunded'),
    ('shipped', 'Shipped'),
    ('delivered', 'Delivered'),
    ('cancelled', 'Cancelled'),
    )

class OrderManagerQuerySet(models.query.QuerySet):
    def by_request(self, request):
        billing_profile, created = BillingProfile.objects.new_or_get(request)
        return self.filter(billing_profile=billing_profile)

    def not_created(self):
        return self.exclude(status='created')

class OrderManager(models.Manager):

    def get_queryset(self):
        return OrderManagerQuerySet(self.model, using=self._db)

    def by_request(self, request):
        return self.get_queryset().by_request(request)

    def new_or_get(self, billing_profile, cart):
        qs = Order.objects.filter(billing_profile=billing_profile,
                                     cart=cart, active=True)
        created = False
        if qs.count() == 1:
            obj = qs.first()
        else:
            obj = Order.objects.create(cart=cart, billing_profile=billing_profile)
            created = True
        return obj, created

class Order(models.Model):
    order_id          = models.CharField(max_length=100, blank=True)
    billing_profile   = models.ForeignKey(BillingProfile, null=True, blank=True, on_delete=models.CASCADE)
    billing_address   = models.ForeignKey(Address, related_name='billing',
                                                 on_delete=models.CASCADE, 
                                                 null=True, blank=True)
    shipping_address  = models.ForeignKey(Address, related_name='shipping',
                                                 on_delete=models.CASCADE, 
                                                 null=True, blank=True)
    cart              = models.ForeignKey(Cart, on_delete=models.CASCADE)
    status            = models.CharField(max_length=100, default='created', choices=ORDER_STATUS_CHOICES)
    shipping_total    = models.DecimalField(default=5.99, decimal_places=2, max_digits=100)
    total             = models.DecimalField(default=0.00, decimal_places=2, max_digits=100)
    active            = models.BooleanField(default=True)
    created_at        = models.DateField(auto_now_add=True)
    updated_at        = models.DateField(auto_now=True)

    def __str__(self):
        return self.order_id

    class Mets:
        ordering = ['-created_at', '-updated_at']

    objects = OrderManager()

    def update_total(self):
        cart_total = self.cart.total
        shipping_total = self.shipping_total
        new_total = fsum([cart_total, shipping_total])
        fomated_total = format(new_total, ".2f")
        self.total = new_total
        self.save()
        return new_total

    def get_absolute_url(self):
        return reverse("orders:detail", kwargs={'order_id': self.order_id})

    # def get_status(self):
    #     if self.status == "refunded":
    #         return "Refunded order"
    #     elif self.status == "shipped":
    #         return "Shipped"
    #     return "Shipping Soon"

    def check_done(self):
        if self.billing_profile and self.billing_address and self.shipping_address and self.total > 0:
            return True
        return False

    def mark_paid(self):
        if self.check_done():
            self.status = "paid"
            self.save()
        return self.status

def pre_save_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
        instance.order_id = unique_order_id_generator(instance)
        order_qs = Order.objects.filter(cart=instance.cart).exclude(billing_profile=instance.billing_profile)
        if order_qs.exists():
            order_qs.update(active=False)

pre_save.connect(pre_save_order_id, Order)

def post_save_cart_total(sender, instance, created, *args, **kwargs):
    if not created:
        cart_id = instance.id
        qs = Order.objects.filter(cart__id=cart_id)
        if qs.count() == 1:
            order_obj = qs.first()
            order_obj.update_total()

post_save.connect(post_save_cart_total, sender=Cart)

def post_save_order(sender, instance, created, *args, **kwargs):
    if created:
        instance.update_total()

post_save.connect(post_save_order, sender=Order)