
from django.contrib import admin
from django.urls import path

from .views import cart_home, cart_update, cart_checkout, checkout_success

urlpatterns = [
    path('', cart_home, name='home'),
    path('checkout/', cart_checkout, name='checkout'),
    path('checkout/success', checkout_success, name='success'),
    path('update/', cart_update, name='update'),
]