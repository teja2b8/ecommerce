from decimal import Decimal
from django.db import models
from django.conf import settings
from django.db.models.signals import *

from products.models import Product

User = settings.AUTH_USER_MODEL

class CartManger(models.Manager):
    def new_or_get(self, request):
        cart_id = request.session.get("cart_id", None)
        qs = Cart.objects.filter(id=cart_id)
        if qs.count() == 1:
            new_ob = False
            cart_obj = qs.first()
            if cart_obj.user is None and request.user.is_authenticated:
                cart_obj.user = request.user
                cart_obj.save()
        else:
            new_ob = True
            cart_obj = Cart.objects.new(user=request.user)
            request.session['cart_id'] = cart_obj.id
        return cart_obj, new_ob

    def new(self, user=None):
        usr_obj=None
        if user is not None:
            if user.is_authenticated:
                usr_obj=user
        return Cart.objects.create(user=usr_obj)

class Cart(models.Model):
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    product = models.ManyToManyField(Product, blank=True)
    sub_total = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    total = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    objects = CartManger()

    def __str__(self):
        return str(self.id)


def m2m_change_cart_receiver(sender, instance, action, *args, **kwargs):
    if action in ('post_add', 'post_remove', 'post_clear'):
        products = instance.product.all()
        total=0
        for p in products:
            total+=p.price
        instance.sub_total = total
        instance.save()

m2m_changed.connect(m2m_change_cart_receiver, sender=Cart.product.through)

def pre_save_cart_receiver(sender, instance, *args, **kwargs):
    if instance.sub_total > 0:
        instance.total = float(Decimal(instance.sub_total) * Decimal(1.08))
    else:
        instance.total = 0.00

pre_save.connect(pre_save_cart_receiver, sender=Cart)
