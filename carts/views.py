from django.conf import settings
from django.shortcuts import render, redirect
from django.http import JsonResponse
from accounts.forms import LoginForm, GuestForm
from addresses.forms import AddressForm
from addresses.models import Address
from billing.models import BillingProfile
from products.models import Product
from .models import Cart
from orders.models import Order
# Create your views here.

import stripe
STRIPE_API_KEY = getattr(settings, 'STRIPE_API_KEY', "sk_test_FwYKvVTXyREQocEco8DvpMHl")
STRIPE_PUB_KEY = getattr(settings, 'STRIPE_PUB_KEY', "pk_test_8NgjOc0Ixb2zwKjJeAq1LBn7")

def cart_detais_api_view(request):
	cart_obj, new_obj = Cart.objects.new_or_get(request)
	products = [{
				'id':obj.id,
				'name':obj.title,
				'price':obj.price,
				'url':obj.get_absolute_url(),
				}
				 for obj in cart_obj.product.all()]
	cart_data = {'products':products, 'sub_total':cart_obj.sub_total, 'total':cart_obj.total}
	print(cart_data)
	return JsonResponse(cart_data)

def cart_home(request):
	cart_obj, new_obj = Cart.objects.new_or_get(request)
	return render(request, "carts/cart_home.html", context={'cart':cart_obj})

def cart_update(request):
	product_id = request.POST.get('product_id')
	print(product_id)
	if product_id is not None:
		try:
			product_obj = Product.objects.get(id=product_id)
		except Product.DoesNotExist:
			raise("Product Not Found")
			return redirect("carts:home")
		cart_obj, new_obj = Cart.objects.new_or_get(request)
		if product_obj in cart_obj.product.all():
			cart_obj.product.remove(product_obj)
			added = False
		else:
			cart_obj.product.add(product_obj)
			added = True
		request.session['cart_items'] = cart_obj.product.count()
		if request.is_ajax():
			json_data = {
				'added' : added,
				'removed' : not added,
				'cart_count' : cart_obj.product.count(),
			}
			return JsonResponse(json_data)
	return redirect("carts:home")

def cart_checkout(request):
	cart_obj , cart_created = Cart.objects.new_or_get(request)
	order_obj = None
	if cart_created or cart_obj.product.count() == 0:
		return redirect("carts:home")

	login_form = LoginForm()
	guest_form = GuestForm()

	billing_profile, billing_created = BillingProfile.objects.new_or_get(request)
	billing_address_id = request.session.get('billing_address_id', None)
	shipping_address_id = request.session.get('shipping_address_id', None)
	address_qs=None
	has_card = False
	if billing_profile is not None:
		if request.user.is_authenticated:
			address_qs = Address.objects.filter(billing_profile=billing_profile)
		order_obj, order_created = Order.objects.new_or_get(billing_profile, cart_obj)
		if shipping_address_id:
			order_obj.shipping_address = Address.objects.get(id=shipping_address_id)
			del request.session['shipping_address_id']
		if billing_address_id:
			order_obj.billing_address = Address.objects.get(id=billing_address_id)
			del request.session['billing_address_id']
		if shipping_address_id or billing_address_id:
			order_obj.save()
		has_card = billing_profile.has_card

	if request.method == 'POST':
		is_done = order_obj.check_done()
		if is_done:
			charged, charge_msg = billing_profile.charge(order_obj)
			if charged:
				order_obj.mark_paid()
				request.session['cart_items'] = 0
				del request.session['cart_id']
				if not billing_profile.user:
					billing_profile.set_cards_inactive()
				return redirect('carts:success')
			else:
				return redirect('carts:checkout')

	address_form = AddressForm()
	context = {
		'billing_profile' : billing_profile,
		'object' : order_obj,
		'login_form' : login_form,
		'guest_form' : guest_form,
		'address_form' : address_form,
		'address_qs': address_qs,
		'has_card' : has_card,
		'pub_key' : STRIPE_PUB_KEY,
		'next_url' : request.build_absolute_uri
		}
	if order_obj:
		if not has_card and order_obj.billing_address and order_obj.shipping_address:
			return redirect("billing-payment")
	return render(request, "carts/checkout.html", context)

def checkout_success(request):
	return render(request, "carts/checkout_success.html", {})