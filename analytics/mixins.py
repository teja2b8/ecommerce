from .signals import object_viewd_signal

class ObjectViewedMixin(object):
	def get_context_data(self, *args, **kwargs):
		context_data = super(ObjectViewedMixin, self).get_context_data(*args, **kwargs)
		instance = context_data.get('object')
		if instance:
			object_viewd_signal.send(instance.__class__, instance=instance, request=self.request)
		return context_data