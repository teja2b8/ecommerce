from django.shortcuts import render
from django.views.generic import ListView
from products.models import Product

# Create your views here.
class SearchListView(ListView):
	template_name ="search/search.html"

	def get_context_data(self, *args, **kwargs):
		context = super(SearchListView, self).get_context_data(*args, **kwargs)
		query = self.request.GET.get('q')
		if query:
			context['query'] = query
		return context

	def get_queryset(self, *args, **kwargs):
		query = self.request.GET.get('q', None)
		queryset = None
		if query:
			queryset = Product.objects.search(query)
		return queryset