from django.conf import settings
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.utils.http import is_safe_url
from .models import BillingProfile, Card

import stripe

STRIPE_API_KEY = getattr(settings, 'STRIPE_API_KEY', "sk_test_FwYKvVTXyREQocEco8DvpMHl")
STRIPE_PUB_KEY = getattr(settings, 'STRIPE_PUB_KEY', "pk_test_8NgjOc0Ixb2zwKjJeAq1LBn7")

stripe.api_key = STRIPE_API_KEY

def billing_payment_view(request):
	next_url = None
	# next_ = request.GET.get('next')
	billing_profile, billing_created = BillingProfile.objects.new_or_get(request)
	if not billing_profile:
		return redirect('/cart')
	# if is_safe_url(next_, request.get_host()):
	# 	next_url = next_
	return render(request, "billing/payment.html", {'pub_key':STRIPE_PUB_KEY, 'next_url':next_url})

def billing_payment_create_view(request):
	if request.method == 'POST' and request.is_ajax():
		print("billing view --------------------")
		billing_profile, billing_created = BillingProfile.objects.new_or_get(request)
		if not billing_profile:
			return HttpResponse({'message':'No User Found'}, status_code=401)		
		token = request.POST.get('token')
		print(token , "-----------token")
		if token is not None:
			card_obj = Card.objects.add_new(billing_profile, token)
		return JsonResponse({'message':'Card Added Successfully'})
	return HttpResponse('error', status_code=401)
