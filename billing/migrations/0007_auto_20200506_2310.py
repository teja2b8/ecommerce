# Generated by Django 3.0.4 on 2020-05-06 17:40

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('billing', '0006_charge'),
    ]

    operations = [
        migrations.AddField(
            model_name='card',
            name='active',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='card',
            name='created_at',
            field=models.DateField(auto_now_add=True, default=datetime.datetime(2020, 5, 6, 17, 40, 53, 828223, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='card',
            name='updated_at',
            field=models.DateField(auto_now=True),
        ),
    ]
