from django.db import models
from django.shortcuts import render
from django.conf import settings
from django.urls import reverse
from django.db.models.signals import post_save, pre_save
from accounts.models import GuestEmail

import stripe
stripe.api_key = "sk_test_FwYKvVTXyREQocEco8DvpMHl"

User = settings.AUTH_USER_MODEL

class BillingProfileManager(models.Manager):
	def new_or_get(self, request):
		guest_id = request.session.get('guest_email_id', None)
		user = request.user
		obj_created = False
		obj = None
		if user.is_authenticated:
			obj, obj_created = self.model.objects.get_or_create(user=user, email=user.email)
		elif guest_id:
			email_obj = GuestEmail.objects.get(id=guest_id)
			obj, obj_created = self.model.objects.get_or_create(email=email_obj.email)
		else:
			pass
		return obj, obj_created

class BillingProfile(models.Model):
	user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)
	email = models.EmailField()
	active = models.BooleanField(default=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	customer_id = models.CharField(max_length=120, null=True, blank=True)

	def __str__(self):
		return self.email

	objects = BillingProfileManager()

	def charge(self, order_obj, card=None):
		return Charge.objects.do(self, order_obj, card)

	def get_cards(self):
		return self.card_set.all()

	def get_payment_method_url(self):
		return reverse('billing-payment')

	@property
	def has_card(self):
		cards = self.get_cards()
		return cards.exists()
	
	@property
	def default_card(self):
		default_card = None
		default_cards = self.get_cards().filter(active=True, default=True)
		if default_cards.exists():
			default_card = default_cards.first()
		return default_card

	def set_cards_inactive(self):
		cards_qs = self.get_cards()
		cards_qs.update(active=False)
		return cards_qs.filter(active=True).count()
	
def billing_presave_stripe_customer(sender, instance, *args, **kwargs):
	if not instance.customer_id and instance.email:
		customer = stripe.Customer.create(
				email = instance.email,
				name = "sample",
				address = {
			    'line1': '510 Townsend St',
			    'postal_code': '98140',
			    'city': 'San Francisco',
			    'state': 'CA',
			    'country': 'US',
			  }
			)
		print(customer)
		instance.customer_id = customer.id

pre_save.connect(billing_presave_stripe_customer, sender=BillingProfile)

def user_created_receiver(sender, instance, created, *args, **kqwargs):
	if created and instance.email:
		BillingProfile.objects.get_or_create(user=instance, email=instance.email)

post_save.connect(user_created_receiver, sender=User)


class CardManager(models.Manager):
	def all(self, *args, **kwargs):
		return self.get_queryset().filter(active=True)

	def add_new(self, billing_profile, token):
		customer = stripe.Customer.retrieve(billing_profile.customer_id)
		stripe_card_response = customer.sources.create(source=token)
		if token:
			new_card = self.model(
					billing_profile = billing_profile,
					card_id = stripe_card_response.id,
					brand = stripe_card_response.brand,
					country = stripe_card_response.country,
					exp_month = stripe_card_response.exp_month,
					exp_year = stripe_card_response.exp_year,
					last4 = stripe_card_response.last4
				)
			new_card.save()
			return new_card
		return None

class Card(models.Model):
	billing_profile = models.ForeignKey(BillingProfile, on_delete=models.CASCADE)
	card_id 		= models.CharField(max_length=120)
	brand 			= models.CharField(max_length=120, null=True, blank=True)
	country 		= models.CharField(max_length=20, null=True, blank=True)
	exp_month 		= models.IntegerField(null=True, blank=True)
	exp_year 		= models.IntegerField(null=True, blank=True)
	last4 			= models.CharField(max_length=4, null=True, blank=True)
	default 		= models.BooleanField(default=True)
	active	 		= models.BooleanField(default=True)
	created_at 		= models.DateField(auto_now_add=True)
	updated_at 		= models.DateField(auto_now=True)

	objects = CardManager()

	def __str__(self):
		return f"{self.brand}-{self.last4}"

def new_card_post_save_Receiver(sender, instance, created, *args, **kwargs):
	if instance.default:
		billing_profile = instance.billing_profile
		qs = Card.objects.filter(billing_profile=billing_profile).exclude(pk=instance.pk)
		qs.update(default=False)

post_save.connect(new_card_post_save_Receiver, sender=Card)


class ChargeManager(models.Manager):
	def do(self, billing_profile,order_obj, card=None):
		card_obj = card
		if card_obj is None:
			cards = billing_profile.card_set.filter(default=True)
			if cards.exists():
				card_obj = cards.first()
		if card_obj is None:
			return False, "No card available"
		charge_obj = stripe.Charge.create(
				amount = int(order_obj.total * 100),
				customer = billing_profile.customer_id,
				currency = 'usd',
				source = card_obj.card_id,
				description = str(order_obj.order_id),
				metadata = {"order_id" : order_obj.order_id},
			)
		new_charge_obj = self.model(
				billing_profile = billing_profile,
				card_id = charge_obj.id,
				paid = charge_obj.paid,
				refunded = charge_obj.refunded,
				outcome = charge_obj.outcome,
				outcome_type = charge_obj.outcome['type'],
				seller_mesage = charge_obj.outcome.get('seller_mesage'),
				risk_level = charge_obj.outcome.get('risk_level')
			)
		new_charge_obj.save()
		return new_charge_obj.paid, new_charge_obj.seller_mesage


class Charge(models.Model):
	billing_profile = models.ForeignKey(BillingProfile, on_delete=models.CASCADE)
	card_id 		= models.CharField(max_length=120, null=True, blank=True)
	paid			= models.BooleanField(default=False)
	refunded		= models.BooleanField(default=False)
	outcome 		= models.CharField(max_length=200, null=True, blank=True)
	outcome_type	= models.CharField(max_length=200, null=True, blank=True)
	seller_mesage	= models.CharField(max_length=300, null=True, blank=True)
	risk_level		= models.CharField(max_length=200, null=True, blank=True)

	objects = ChargeManager()

	def __str__(self):
		return self.billing_profile.email